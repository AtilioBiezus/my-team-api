package br.edu.unisep.myteam.controller.player;

import br.edu.unisep.myteam.domain.dto.player.RegisterPlayerDto;
import br.edu.unisep.myteam.domain.usecase.RegisterPlayerUseCase;
import br.edu.unisep.myteam.response.DefaultResponse;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@AllArgsConstructor
@RestController
@RequestMapping("/player")
public class PlayerController {

    private final RegisterPlayerUseCase registerPlayer;

    @PostMapping
    public ResponseEntity<DefaultResponse<Boolean>> save(@RequestBody RegisterPlayerDto player) {
        registerPlayer.execute(player);
        return ResponseEntity.ok(DefaultResponse.of(true));
    }

}
