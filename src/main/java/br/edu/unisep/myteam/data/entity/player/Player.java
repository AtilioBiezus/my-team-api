package br.edu.unisep.myteam.data.entity.player;

import br.edu.unisep.myteam.data.entity.team.Team;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "player")
public class Player {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer player_id;

    @Column(name = "name")
    private String player_name;

    @Column(name = "age")
    private Integer age;

    @Column(name = "nationality")
    private String nationality;

    @Column(name = "position")
    private String position;

    @OneToOne
    @JoinColumn(name = "team_id", referencedColumnName = "team_id")
    private Team teamId;
}
