package br.edu.unisep.myteam.data.entity.team;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "team")
public class Team {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer team_id;

    @Column(name = "name")
    private String team_name;
}