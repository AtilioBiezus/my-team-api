package br.edu.unisep.myteam.data.repository;


import br.edu.unisep.myteam.data.entity.team.Team;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TeamRepository extends JpaRepository<Team, Integer> {
}
