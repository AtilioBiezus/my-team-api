package br.edu.unisep.myteam.domain.builder;

import br.edu.unisep.myteam.data.entity.player.Player;
import br.edu.unisep.myteam.domain.dto.player.PlayerDto;
import br.edu.unisep.myteam.domain.dto.player.RegisterPlayerDto;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class PlayerBuilder {

    public List<PlayerDto> from(List<Player> players) {
        return players.stream().map(this::from).collect(Collectors.toList());
    }

    public PlayerDto from(Player player) {
        return new PlayerDto(
          player.getPlayer_id(),
          player.getPlayer_name(),
          player.getAge(),
          player.getNationality(),
          player.getPosition()
        );
    }

    public Player from(RegisterPlayerDto registerPlayer) {
        Player player = new Player();
        player.setPlayer_name(registerPlayer.getName());
        player.setAge(registerPlayer.getAge());
        player.setNationality(registerPlayer.getNationality());
        player.setPosition(registerPlayer.getPosition());

        return player;
    }

}
