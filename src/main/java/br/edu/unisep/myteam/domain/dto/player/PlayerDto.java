package br.edu.unisep.myteam.domain.dto.player;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class PlayerDto {

    private Integer id;

    private String name;

    private Integer age;

    private String nationality;

    private String position;

}
