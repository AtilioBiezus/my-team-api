package br.edu.unisep.myteam.domain.dto.player;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RegisterPlayerDto {

    private String name;

    private Integer age;

    private String nationality;

    private String position;

}
