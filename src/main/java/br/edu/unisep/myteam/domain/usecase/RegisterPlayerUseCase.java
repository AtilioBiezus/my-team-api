package br.edu.unisep.myteam.domain.usecase;

import br.edu.unisep.myteam.data.repository.PlayerRepository;
import br.edu.unisep.myteam.domain.builder.PlayerBuilder;
import br.edu.unisep.myteam.domain.dto.player.RegisterPlayerDto;
import br.edu.unisep.myteam.domain.validator.player.RegisterPlayerValidator;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class RegisterPlayerUseCase {

    private final RegisterPlayerValidator validator;
    private final PlayerBuilder builder;
    private final PlayerRepository playerRepository;

    public void execute(RegisterPlayerDto registerPlayer) {
        validator.validate(registerPlayer);

        var player = builder.from(registerPlayer);
        playerRepository.save(player);
    }

}
