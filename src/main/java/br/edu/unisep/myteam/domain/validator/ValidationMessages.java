package br.edu.unisep.myteam.domain.validator;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ValidationMessages {

    public static final String MESSAGE_REQUIRED_PLAYER_ID = "Informe o id do jogador!";
    public static final String MESSAGE_REQUIRED_PLAYER_NAME = "Informe o nome do jogador!";
    public static final String MESSAGE_REQUIRED_PLAYER_AGE = "Informe a idade do jogador!";
    public static final String MESSAGE_REQUIRED_PLAYER_NATIONALITY = "Informe a nacionalidade do jogador!";
    public static final String MESSAGE_REQUIRED_PLAYER_POSITION = "Informe a posição do jogador!";


}
