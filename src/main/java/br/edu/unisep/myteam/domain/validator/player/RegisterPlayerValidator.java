package br.edu.unisep.myteam.domain.validator.player;

import br.edu.unisep.myteam.domain.dto.player.RegisterPlayerDto;
import org.apache.commons.lang3.Validate;
import org.springframework.stereotype.Component;

import static br.edu.unisep.myteam.domain.validator.ValidationMessages.*;

@Component
public class RegisterPlayerValidator {

    public void validate(RegisterPlayerDto registerPlayer) {
        Validate.notBlank(registerPlayer.getName(), MESSAGE_REQUIRED_PLAYER_NAME);
        Validate.notNull(registerPlayer.getAge(), MESSAGE_REQUIRED_PLAYER_AGE);
        Validate.notBlank(registerPlayer.getNationality(), MESSAGE_REQUIRED_PLAYER_NATIONALITY);
        Validate.notBlank(registerPlayer.getPosition(), MESSAGE_REQUIRED_PLAYER_POSITION);

    }

}
